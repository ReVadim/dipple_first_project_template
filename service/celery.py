import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'service.settings')

app = Celery('service', broker_connection_max_retries=5)
app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()
