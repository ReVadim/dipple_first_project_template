import time

from celery.signals import task_postrun

from service import celery_app
from service.apps.examples.models import ExampleAsyncTask


def get_task(pk: int) -> ExampleAsyncTask | None:
    try:
        task = ExampleAsyncTask.objects.get(pk=pk)
    except ExampleAsyncTask.DoesNotExist:
        return None

    return task


@celery_app.task()
def update_task(pk: int):
    task = get_task(pk)

    if task is None:
        return None

    time.sleep(task.one_percent_delay_milliseconds / 1000)

    task = get_task(pk)

    if task is None:
        return None

    task.percentage += 1
    task.save()

    return pk


@task_postrun.connect(sender=update_task)
def task_post_run_handler(sender=None, **kwargs):
    pk = kwargs.get('retval', None)

    if pk is None:
        return

    task = get_task(pk)

    if task is None:
        return

    if task.percentage < 100:
        update_task.delay(pk)
