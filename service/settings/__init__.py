from os import environ

import django_stubs_ext
from split_settings.tools import include, optional

DJANGO_ENV = 'DJANGO_ENV'

# stubs for mypy
# https://pypi.org/project/django-stubs-ext/
django_stubs_ext.monkeypatch()

environ.setdefault(DJANGO_ENV, 'development')
_ENV = environ[DJANGO_ENV]
_base_settings = (
    'components/common.py',
    'components/logging.py',
    'components/csp.py',
    'components/redis.py',
    'components/caches.py',
    'components/celery.py',

    'environments/{0}.py'.format(_ENV),

    optional('environments/local.py'),
)

include(*_base_settings)
