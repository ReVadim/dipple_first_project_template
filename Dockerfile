FROM python:3.11.3

WORKDIR ~/

RUN pip install --upgrade pip setuptools
RUN pip install poetry
COPY ./pyproject.toml ./
RUN poetry config virtualenvs.create false && poetry install --no-root
