# Установка Docker-desktop

[Назад](../non-project-dev-tools.md)

Добавить репозиторий

```shell
sudo apt update && sudo apt install -y ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Скачать и установить deb пакет. 
Для этого идем на страницу [Docker Desktop release notes](https://docs.docker.com/desktop/release-notes/)
и копируем ссылку на последнюю версию для Debian. 

У меня это 4.19.0 [https://desktop.docker.com/linux/main/amd64/docker-desktop-4.19.0-amd64.deb](https://desktop.docker.com/linux/main/amd64/docker-desktop-4.19.0-amd64.deb)

```shell
mkdir ~/downloads
cd ~/downloads
wget https://desktop.docker.com/linux/main/amd64/docker-desktop-4.19.0-amd64.deb
sudo apt update && sudo apt install -y ./docker-desktop-4.19.0-amd64.deb
cd ~
rm -fr ~/downloads
```

Запускаем

```shell
sudo usermod -aG docker $USER
newgrp docker
systemctl --user start docker-desktop
```

Проверяем

```shell
docker compose version
docker --version
docker version
```

Должен вывести примерно следующие
```shell
Client: Docker Engine - Community
 Cloud integration: v1.0.31
 Version:           24.0.2
 API version:       1.41 (downgraded from 1.43)
 Go version:        go1.20.4
 Git commit:        cb74dfc
 Built:             Thu May 25 21:51:00 2023
 OS/Arch:           linux/amd64
 Context:           desktop-linux

Server: Docker Desktop 4.14.1 (91661)
 Engine:
  Version:          20.10.21
  API version:      1.41 (minimum version 1.12)
  Go version:       go1.18.7
  Git commit:       3056208
  Built:            Tue Oct 25 18:00:19 2022
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.6.9
  GitCommit:        1c90a442489720eec95342e1789ee8a5e1b9536f
 runc:
  Version:          1.1.4
  GitCommit:        v1.1.4-0-g5fd4c4d
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
 grm@grm-work-host  ~/Downloads  systemctl --user start docker-desktop
 grm@grm-work-host  ~/Downloads  docker compose version
docker --version
docker version
Docker Compose version v2.17.3
Docker version 24.0.2, build cb74dfc
Client: Docker Engine - Community
 Cloud integration: v1.0.31
 Version:           24.0.2
 API version:       1.42 (downgraded from 1.43)
 Go version:        go1.20.4
 Git commit:        cb74dfc
 Built:             Thu May 25 21:51:00 2023
 OS/Arch:           linux/amd64
 Context:           desktop-linux

Server: Docker Desktop 4.19.0 (106363)
 Engine:
  Version:          23.0.5
  API version:      1.42 (minimum version 1.12)
  Go version:       go1.19.8
  Git commit:       94d3ad6
  Built:            Wed Apr 26 16:17:45 2023
  OS/Arch:          linux/amd64
  Experimental:     false
 containerd:
  Version:          1.6.20
  GitCommit:        2806fc1057397dbaeefbea0e4e17bddfbd388f38
 runc:
  Version:          1.1.5
  GitCommit:        v1.1.5-0-gf19387a
 docker-init:
  Version:          0.19.0
  GitCommit:        de40ad0
 ```

Готово

[Назад](../non-project-dev-tools.md)
