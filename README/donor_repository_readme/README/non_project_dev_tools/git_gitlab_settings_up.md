# Настройка git, GitLab

[Назад](../non-project-dev-tools.md)

## Настройка git

```shell
git config --global user.email "<YOU_EMAIL>"
git config --global user.name "<YOU_USERNAME>"
```

## Создание ключа ssh

```shell
mkdir -p ~/.ssh/gitlab
ssh-keygen -t rsa -b 2048 -C "<YOU_EMAIL>" -f ~/.ssh/gitlab/id_rsa
chmod 600 ~/.ssh/gitlab/id_rsa
chmod 600 ~/.ssh/gitlab/id_rsa.pub
echo -e "Host gitlab.com\n  PreferredAuthentications publickey\n  IdentityFile ~/.ssh/gitlab/id_rsa\n" >> ~/.ssh/config
```

## Копируем публичный ключ

```shell
cat ~/.ssh/gitlab/id_rsa.pub | xclip -selection c
```

## Вставляем (ctrl+v) его в gitlab по ссылке

[https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys)

![gitlab add ssh key](gitlab-add-ssh-key.png)

Жмякаем на Add key

## Проверяем

```shell
ssh git@gitlab.com
```

Должно вывестись сообщение наподобие этого

```shell
PTY allocation request failed on channel 0
Welcome to GitLab, @golosovsa!
Connection to gitlab.com closed.
```

Если что-то пошло не так, пробуем перезайти в систему или перезагрузиться

## Если надо переехать...

... то забираем с собой папку ~/.ssh

[Назад](../non-project-dev-tools.md)
