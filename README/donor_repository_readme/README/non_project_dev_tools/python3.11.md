# Установка Python3.11 

[Назад](../non-project-dev-tools.md)

Идем на [оф сайт питона](https://www.python.org/downloads/source/) и выбираем Latest Python 3 Release.

У меня это [Python source release 3.11.3](https://www.python.org/ftp/python/3.11.3/Python-3.11.3.tar.xz)

Устанавливаем из исходников в домашнюю папку

```shell
sudo apt update -y
sudo apt -y install zlib1g-dev build-essential libgdbm-dev libncurses5-dev libssl-dev libnss3-dev libffi-dev libreadline-dev wget libsqlite3-dev libbz2-dev git
mkdir ~/downloads
cd ~/downloads/
git clone git://git.openssl.org/openssl.git
cd openssl
./config --prefix=$HOME/.local/openssl --openssldir=$HOME/.local/openssl
make
make install
cd ..
rm -fr *
wget https://www.python.org/ftp/python/3.11.3/Python-3.11.3.tar.xz
tar xvf *.tar.xz
rm *.tar.xz
cd Python-3.11.3
./configure --prefix=$HOME/.local/python --enable-optimizations --with-openssl=$HOME/.local/openssl/
make
make install
cd ~
rm -fr ~/downloads
ln $HOME/.local/python/bin/pip3 $HOME/.local/python/bin/pip
```

Определяем что за терминал у нас используется (да просто так)

```shell
echo $SHELL
```

В домашней папке пользователя должен быть файл настроек вашего терминала `.<НАЗВАНИЕ ТЕРМИНАЛА>rc`

```shell
ls -a ~ | grep $(echo $SHELL | sed 's/\/.*\//\./')
```

Добавляем питон в $PATH. 
Открываем файл пользовательских настроек терминала `.profile`. В конец файла дописываем:

```shell
# Python
PYTHONPATH=$HOME/.local/python
PATH=$PYTHONPATH:$PYTHONPATH/bin:$PATH
```

Перезайдем под своим пользователем в систему, проверяем

```shell
which python3
which pip
python3 --version
```

Должно быть 

```shell
/home/<ПОЛЬЗОВАТЕЛЬ>/.local/python/bin/python3
/home/<ПОЛЬЗОВАТЕЛЬ>/.local/bin/pip3
Python 3.11.3
```

Если не получилось - попробуй перезагрузиться

Обновим pip

```shell
pip install --upgrade pip setuptools cffi
```

Все, хватит

[Назад](../non-project-dev-tools.md)

