# Инструкция по изменению настроек проекта

[Назад](../README.md)

## Дерево каталогов

```
.
├── config
│   ├── .env.local
│   └── .env.win.local
├── docker-dev
│   └── docker-compose.yml
├── for-windows-users
│   ├── docker-compose.yml
│   └── Dockerfile
├── README
│   ├── ...
├── service
│   ├── apps
│   │   ├── ...
│   ├── settings
│   │   ├── components
│   │   │   ├── caches.py
│   │   │   ├── common.py
│   │   │   ├── csp.py
│   │   │   ├── __init__.py
│   │   │   ├── logging.py
│   │   ├── environments
│   │   │   ├── development.py
│   │   │   ├── __init__.py
│   │   │   ├── production.py
│   │   ├── __init__.py
│   ├── templates
│   │   └── ...
│   ├── asgi.py
│   ├── __init__.py
│   ├── urls.py
│   └── wsgi.py
├── README.md
├── .editorconfig
├── .gitignore
├── manage.py
├── nitpick-style.toml
├── poetry.lock
├── .pre-commit-config.yaml
├── pyproject.toml
└── setup.cfg
```

## Добавление/изменение настроек редактора

Настройки отображения для редакторов кода расположены в файле [.editorconfig](../../../.editorconfig)

Такие редакторы как VSCode или PyCharm автоматически увидят этот файл и применят настройки

## Добавление/изменение настроек линтеров

Изменение настроек различных dev пакетов собрано в один файл конфигураций, и разворачивается с помощью утилиты nitpick

### nitpick

Это консольная утилита, позволяющая гибко объединять настройки разных 
помагаторов (линтеров, форматеров, и прочих) в одном месте. 
Можно даже в другой репе, но нам пока этого не надо.

Для пользования надо знать всего пару команд.

### check

```shell
nitpick check
```

Проверяет соответствие настроек в проекте заданным требованиям

### fix

```shell
nitpick fix
```

Вносит изменения в настройки проекта в соответствие с требованиями

### Настройки nitpick

Настройки могут быть размещены в файле pyproject.toml, удаленном репозитории
или в файле nitpick-style.toml. В этом проекте все настройки находятся в
файле [nitpick-style.toml](../../../nitpick-style.toml)

## Добавление/изменение git хуков

### pre-commit

Это консольная утилита, позволяющая настраивать события изменения репозитория, 
а именно, выполнять какие-либо действия до или после команд `git commit`, `git push`, `git merge` и тп.
Может быть частью ci/cd процесса

В данном проекте используется для запуска линтеров до коммита, 
прерывает коммит, если линтеры обнаружили ошибки.

Для пользования надо знать всего пару команд.

## install
```shell
pre-commit install
```

Установит хуки внутри локального репозитория

## uninstall

```shell
pre-commit uninstall
```

Удалит хуки из локального репозитория

### Настройки pre-commit

Все настройки для пре-коммитов находятся в файле [nitpick-style.toml](../../../nitpick-style.toml),
После развертывания nitpick командой `nitpick fix`, автоматически создастся файл настроек [.pre-commit-config.yaml](../../../.pre-commit-config.yaml)

[Назад](../README.md)
