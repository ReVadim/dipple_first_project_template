# Подготовка рабочего окружения

[Назад](../README.md)

1. [Установить Ubuntu](https://help.ubuntu.ru/wiki/ubuntu_install)
2. Обновить `sudo apt update && sudo apt upgrade -y`
3. Настроить себе что-нибудь, или нет (опционально)
4. [Установить питон](non_project_dev_toolsython3.11.md)
5. [Установить poetry](non_project_dev_toolsoetry.md)
6. [Установить PyCharm](non_project_dev_toolsycharm.md)
7. [Установить Docker Desktop](non_project_dev_toolsocker-desktop.md)
8. [Настроить git, gitlab](non_project_dev_toolsit_gitlab_settings_up.md)
